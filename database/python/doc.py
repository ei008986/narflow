from spacy.tokens import Doc, Span

def split_doc_by_lines(doc: Doc, min_amount: int = 1):
    """
    Split a document into spans, with each span representing a single line.

    Parameters:
    doc (spacy.tokens.Doc): The document to split.
    min_amount (int): The minimum amount of new lines to split on.

    Returns:
    List[spacy.tokens.Span]: A list of spans, where each span represents a single line.
    """
    spans = []
    start = 0
    for i, token in enumerate(doc):
        if ('\n' * min_amount) in token.text:
            if i > start:
                span = doc[start:i]
                spans.append(span)
                start = i
            span = doc[start:i+1]
            spans.append(span)
            start = i + 1
    if start < len(doc):
        span = doc[start:]
        spans.append(span)
    return spans

def split_doc_by_sentences(doc: Doc):
    """
    Split a document into spans, with each span representing a single sentence.
    
    Parameters:
    doc (spacy.tokens.Doc): The document to split.
    
    Returns:
    List[spacy.tokens.Span]: A list of spans, where each span represents a single sentence.
    """
    spans: list[Span] = []
    start = 0
    for token in doc:
        if token.is_sent_start:
            span = doc[start:token.i]
            spans.append(span)
            start = token.i
    if start < len(doc):
        span = doc[start:]
        spans.append(span)
    return spans