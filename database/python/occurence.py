def occurence_matrix(string: str) -> dict:
    """Returns a dictionary of words and their occurences in a string.

    Args:
        string (str): The string to count the occurences of words in.

    Returns:
        dict: A dictionary of words and their occurences in the string.
    """
    # Convert to lowercase
    string = string.lower()
    # Remove all non-alphabetic characters
    words = "".join([word for word in string if word.isalpha() or word.isspace()])
    # Split into words
    words = words.split()
    # Create an empty dictionary
    occurrence_dict = {}
    # Iterate through words
    for word in words:
        # If word is in dictionary, increment its value
        if word in occurrence_dict:
            occurrence_dict[word] += 1
        # Else, add it to the dictionary with a value of 1
        else:
            occurrence_dict[word] = 1
    # Return the dictionary
    return occurrence_dict
