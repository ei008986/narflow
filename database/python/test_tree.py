import unittest
from tree import TreeNode, preorder

class Test_Tree(unittest.TestCase):

    def test_add_child(self):
        root = TreeNode("root", None)
        root.add_child("child")
        self.assertEqual(root.is_leaf(), False)
  
    def test_is_leaf(self):
        root = TreeNode("leaf", None)
        child = root.add_child("child")
        self.assertEqual(child.is_leaf(), True)
        self.assertEqual(root.is_leaf(), False)

    def test_is_root(self):
        root = TreeNode("root", None)
        child = root.add_child("child")
        self.assertEqual(root.is_root(), True)
        self.assertEqual(child.is_root(), False)

    def test_is_only_child(self):
        root = TreeNode("root", None)
        child = root.add_child("child")
        child1 = child.add_child("child1")
        child2 = child.add_child("child2")
        self.assertEqual(child.is_only_child(), True)
        self.assertEqual(child1.is_only_child(), False)

    def test_get_child_index(self):
        root = TreeNode("root", None)
        child1 = root.add_child("child1")
        child2 = root.add_child("child2")
        self.assertEqual(child1.get_child_index(), 0)
        self.assertEqual(child2.get_child_index(), 1)

    def test_is_first_child(self):
        root = TreeNode("root", None)
        child1 = root.add_child("child1")
        child2 = root.add_child("child2")
        self.assertEqual(child1.is_first_child(), True)
        self.assertEqual(child2.is_first_child(), False)
    
    def test_is_last_child(self):
        root = TreeNode("root", None)
        child1 = root.add_child("child1")
        child2 = root.add_child("child2")
        self.assertEqual(child2.is_last_child(), True)
        self.assertEqual(child1.is_last_child(), False)

    def test_get_ancestor_at_level(self):
        root = TreeNode("root", None)
        child = root.add_child("child")
        grandchild = child.add_child("grandchild")
        self.assertEqual(grandchild.get_ancestor_at_level(1), root)
        self.assertEqual(grandchild.get_ancestor_at_level(2), child)
        self.assertEqual(grandchild.get_ancestor_at_level(3), grandchild)

class Test_TreeTraversal(unittest.TestCase):

    def test_preorder(self):
        root = TreeNode("root", None)
        child1 = root.add_child("child1")
        child2 = root.add_child("child2")
        child1_1 = child1.add_child("child1_1")
        child1_2 = child1.add_child("child1_2")
        child2_1 = child2.add_child("child2_1")
        child2_2 = child2.add_child("child2_2")
        tree_list = []
        preorder(root, tree_list)
        self.assertEqual(tree_list, [root, child1, child1_1, child1_2, child2, child2_1, child2_2])

if __name__ == '__main__':
    unittest.main()
