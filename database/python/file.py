import os
import re
import json
from print import print_error

HTML_TAG_PATTERN = "<[^>]*>"

class File:
    def __init__(self, path: str):
        self.path = path
        if self.exists and self.can_read:
            self.data = open(self.path, encoding = "utf-8").read()

    @property
    def name(self):
        return os.path.splitext(self.path)[0].split("/")[-1]

    @property
    def extension(self):
        return os.path.splitext(self.path)[1]

    @property
    def exists(self):
        return os.path.isfile(self.path)

    @property
    def can_read(self):
        return os.access(self.path, os.R_OK)

    @property
    def is_valid(self):
        return self.exists and self.can_read

    def replace_tags(self, tags_list: dict[str, str]):
        """Replaces html tags with a given value. E.g. `{"i" : "*"}` would replace all <i> or </i> tags with asterixes."""
        for tag in tags_list:
            self.data = re.sub(f"<[{tag}|/{tag}>]*>", tags_list[tag], self.data)

    def strip_tags(self):
        """Removes all HTML elements from this file but keep their content. E.g. `<b>Hello</b>` would return `Hello`."""
        self.data = re.sub(HTML_TAG_PATTERN, "", self.data)

    def strip_whitespace(self):
        """Removes multiple newlines and spaces."""
        self.data = re.sub(r"^(\s*(\r\n|\r|\n)){2,}", "\n", self.data, flags=re.M)      # Remove multiple newlines
        self.data = re.sub(r"(?<=\S)( {2,})(?=\S)", " ", self.data)                     # Remove multiple spaces

    def double_chars(self, list_of_chars: "list[str]"):
        """Doubles the characters in the file from the given list. E.g. Given `['g']`, all `'g'`s would turn into `'gg'`."""
        for char in list_of_chars:
            self.data = self.data.replace(char, char * 2)

    def get_json_dict(self):
        """If this file is a json file, returns the data as a dictionary; if not `None` is returned."""
        try:
            return json.loads(self.data)
        except:
            print_error(f"ERROR: {self.name} is not valid JSON!")
            return None

    def split_by_newline(self, amount: int = 1) -> list[str]:
        """
        Splits this file by newline characters (`\\n`) into substrings.

        Args:
            string (str): The string to be split.
            amount (int, optional): How many newlines should be used as the split delimiter. Defaults to 1.

        Returns:
            list[str]: List of substrings. 
        """
        split_delimiter = '\n' * amount
        return self.data.split(split_delimiter)
