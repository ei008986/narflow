import re
from tree import TreeNode
from spacy.tokens import Span

EMPTY_LINE_PATTERN = "\n\n"
POEM_PATTERNS = [EMPTY_LINE_PATTERN]
ACT_X_PATTERN = "ACT .*"
SCENE_X_PATTERN = "SCENE .*"
PLAY_PATTERNS = [ACT_X_PATTERN, SCENE_X_PATTERN]

class SectionType:
    def __init__(self, name: str, delimiter_patterns: "list[str]", is_missing_initial_delimiter: bool, fallback_section_string: str = "Section"):
        self.name = name
        self.delimiter_patterns = delimiter_patterns
        self.is_missing_initial_delimiter = is_missing_initial_delimiter
        self.fallback_section_string = fallback_section_string

poem_type = SectionType("Poem", POEM_PATTERNS, True, "Stanza")
play_type = SectionType("Play", PLAY_PATTERNS, False)

SECTION_TYPES = {"poem": poem_type, "play": play_type}

class SectionTree:
    def __init__(self, token_stream: list[Span], type: SectionType, root_name="Tree") -> None:
        self.token_stream = token_stream
        self.delimiter_patterns = type.delimiter_patterns
        self.root_node = TreeNode(root_name, None)
        self.is_missing_initial_delimiter = type.is_missing_initial_delimiter
        self.fallback_section_string = type.fallback_section_string
        self.section_count = 0
        self.setup_tree(self.root_node)

    def setup_tree(self, current_parent: TreeNode):
        # If there should be a delimiter at the start of the text (typically at the start of a poem where there is no stanza break), add one.
        if self.is_missing_initial_delimiter: 
            current_parent = current_parent.add_child(self.get_delimiter_string(""))
        # Loop through token stream
        for current_stream in self.token_stream:
            current_string = current_stream.text
            is_delimiter, delimiter_desired_level = self.is_section_delimiter(current_string)
            # If a delimiter, add to current parent, then set current parent as itself
            if is_delimiter:
                # Get first parent at the desired delimiter level, add a new delimiter node and set it to current parent
                current_parent = current_parent.get_ancestor_at_level(delimiter_desired_level)
                current_parent = current_parent.add_child(self.get_delimiter_string(current_string))
            # If not a delimiter, add to current parent and continue
            else:
                if self.is_valid_string(current_string):
                    current_parent.add_child(current_stream)

    def get_delimiter_string(self, current_string):
        self.section_count += 1
        return current_string if self.is_valid_string(current_string) else self.fallback_section_string + f" {self.section_count}"

    def is_section_delimiter(self, string: str):
        for index, pattern in enumerate(self.delimiter_patterns):
            if re.match(pattern, string) is not None:
                return True, index + 1
        return False, -1

    def is_valid_string(self, string: str):
        is_empty = string.isspace() or len(string) == 0
        return not is_empty
