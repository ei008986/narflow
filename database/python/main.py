from file import File
from tree import TreeNode
from section import SectionTree, SECTION_TYPES
from database_manager import DatabaseManager
from print import print_error, print_success, print_warning
from sql import add_author_sql, select_work_sql, add_work_sql, select_author_uid_sql, add_passage_sql, add_section_sql, select_section_uid_sql, delete_work_sql, add_image_sql, select_image_uid_sql, add_token_sql, select_passage_uid_sql, add_appearance_sql, add_character_sql, select_character_uid_sql, select_token_uid_sql
from tree import preorder
from narflow import calculate_narrative_flow_score
from os import path, linesep
from occurence import occurence_matrix
from spacy.tokens import Span
from spacy.lang.en.stop_words import STOP_WORDS
from doc import split_doc_by_lines
import en_core_web_trf
import en_core_web_lg
import en_core_web_sm
import sys
import time

PROJECT_NAME = "narflow"
ROOT_DIRECTORY = path.join(__file__.split(PROJECT_NAME)[0], PROJECT_NAME)
DATABASE_CONFIG_FILE = path.join(ROOT_DIRECTORY, "database.ini")
DATABASE_CONFIG_SECTION = "postgresql"
CORPUS_DIRECTORY = path.join(ROOT_DIRECTORY, "database", "corpus")
CORPUS_JSON_FILE = path.join(CORPUS_DIRECTORY, "corpus.json")
SQL_PATH = path.join(ROOT_DIRECTORY, "database", "sql")
CREATE_TABLES_SQL = path.join(SQL_PATH, "create-tables.sql")
DROP_TABLES_SQL = path.join(SQL_PATH, "drop-tables.sql")
INSERT_TOKEN_SQL = path.join(SQL_PATH, "insert-token.sql")

def main():

    main_start_time = time.time()

    print(f"{PROJECT_NAME.title()} database wizard! 🧙")

    # Setup database manager
    config_file = File(DATABASE_CONFIG_FILE)
    if not config_file.exists:
        print_error(f"Database config file {DATABASE_CONFIG_FILE} does not exist.")
        return
    database_manager = DatabaseManager(DATABASE_CONFIG_FILE)

    # Lets read all the data from corpus.json
    print("Acccessing corpus...")
    corpus_data = File(CORPUS_JSON_FILE).get_json_dict()
    if corpus_data is None:
        print_error(f"Corpus file {CORPUS_JSON_FILE} does not exist or is not valid JSON.")
        return
    print_success(f"Successfully got {corpus_data['name']}!")
    corpus_version = corpus_data['version']
    print(f"Corpus Version: {corpus_version}")
    corpus_works = corpus_data['works']

    # Rebase the database
    try:
        should_rebase = True if sys.argv[1] == "rebase" else False
    except IndexError:
        should_rebase = False
    if (should_rebase):
        print("Rebasing database...")
        database_manager.execute_from_file(DROP_TABLES_SQL)

    # Create tables if they don't exist
    database_manager.execute_from_file(CREATE_TABLES_SQL)

    # Setup spacy model
    print("Loading spacy model...")
    nlp = en_core_web_lg.load()
    print_success("Successfully loaded spacy model!")

    # Lets loop through every work in the corpus to add it to the database
    for work in corpus_works:
        # Get all information about the work
        title = work['name']
        author = work['author']
        work_type = work['type']
        work_file = work['file']
        blurb = work.get('blurb')
        genre = work.get('genre')
        date = work.get('date')
        image_url = work.get('image')['url']
        image_attribution = work.get('image')['attribution']

        # Add author if not exists and get author uid
        author_uid = database_manager.execute_fetch(select_author_uid_sql(author))
        if author_uid is None:
            database_manager.execute(add_author_sql(author))
            author_uid = database_manager.execute_fetch(select_author_uid_sql(author))
        if author_uid is not None:
            author_uid = author_uid[0]
            print(f"{author} UID is {author_uid}")

        # Add image to the database
        database_manager.execute(add_image_sql(image_url, image_attribution))
        # Get image UID
        image_uid = database_manager.execute_fetch(select_image_uid_sql(image_url))
        if image_uid is not None:
            image_uid = image_uid[0]

        # Check if work exists in corpus
        work_result = database_manager.execute_fetch(select_work_sql(title))
        if work_result is not None:
            work_uid = work_result[0]
            work_version = work_result[1]
            if work_version == corpus_version:
                print_warning(f"{title} already up to date (v{work_version}), skipping it.")
                continue
            else:
                print(f"Updating {title} from v{work_version} to v{corpus_version}...")
                database_manager.execute(delete_work_sql(work_uid))
        else:
            print(f"Attempting to add {title} to the database...")

        # Add work to the database
        database_manager.execute(add_work_sql(title, author_uid, corpus_version, blurb, genre, date, work_type, image_uid))
        print("Adding work to database...")
        work_uid = database_manager.execute_fetch(select_work_sql(title))
        if work_uid is not None:
            work_uid = work_uid[0]
        print(f"{title} UID is {work_uid}")

        # Clean file
        file = File(path.join(CORPUS_DIRECTORY, work_file))
        file.strip_tags()
        file.strip_whitespace()

        # Get occurence matrix
        occurence = occurence_matrix(file.data)
        non_stop_occurence = occurence.copy()
        
        # Remove stop words from occurence matrix
        for stop_word in STOP_WORDS:
            non_stop_occurence.pop(stop_word, None)

        # Prepare file for going into database
        # file.double_chars(["'"])

        # Convert file to spacy doc
        print("Processing document...")
        start_time = time.time()
        doc = nlp(file.data)
        print_success(f"Successfully processed document in {round(time.time() - start_time, 3)} seconds!")

        # Split document into passages
        passage_list = split_doc_by_lines(doc)

        # Create the section tree
        section_tree = SectionTree(passage_list, SECTION_TYPES[work_type])

        # Traverse through the tree in pre order (depth first, left most nodes visited first)
        traversed_tree: list[TreeNode] = []
        preorder(section_tree.root_node, traversed_tree)

        section_count = 0
        passage_count = 0
        token_count = 0
        
        # Loop through each node in the traversed tree
        for current_index, current_node in enumerate(traversed_tree):
            # Check if this node has a parent
            if current_node.parent is None:
                continue
            # Check if this node is a leaf and therefore needs to be added to passage table
            if current_node.is_leaf():
                # Get parent uid
                parent_uid = database_manager.execute_fetch(select_section_uid_sql(work_uid, current_node.parent.data))
                if parent_uid is not None:
                    parent_uid = parent_uid[0]
                # Get text of passage
                if type(current_node.data) is Span:
                    text: str = current_node.data.text
                else:
                    text = str(current_node.data)
                # Calculate narrative flow score
                narrative_flow_score = None
                if type(current_node.data) is Span:
                    next_text = traversed_tree[current_index + 1].data if current_index + 1 < len(traversed_tree) else None
                    previous_text = traversed_tree[current_index - 1].data if current_index - 1 >= 0 else None
                    if type(next_text) is Span and type(previous_text) is Span:
                        narrative_flow_score = calculate_narrative_flow_score(current_node.data, next_text, previous_text)
                # Add passage to database
                database_manager.execute(add_passage_sql(work_uid, parent_uid, passage_count, text.replace("'", "''"), narrative_flow_score))
                passage_uid = database_manager.execute_fetch(select_passage_uid_sql(work_uid, passage_count))
                if passage_uid is not None:
                    passage_uid = passage_uid[0]
                passage_count += 1

                # Loop through each token in the passage
                token_index = 0

                # This will not tokenise text if a section header
                if type(current_node.data) is not Span:
                    continue

                for token in current_node.data:
                    # See: https://spacy.io/api/token

                    token_uid = database_manager.execute_from_file(INSERT_TOKEN_SQL, 
                                                                   passage_uid = passage_uid,
                                                                   work_uid = work_uid,
                                                                   index = token_index,
                                                                   text = token.text.replace("'", "''"),
                                                                   lemma = token.lemma_.replace("'", "''"),
                                                                   simple_pos = token.pos_.replace("'", "''"),
                                                                   detailed_pos = token.tag_.replace("'", "''"),
                                                                   semantic_dependancy = token.dep_.replace("'", "''"),
                                                                   shape = token.shape_.replace("'", "''"),
                                                                   is_alpha = token.is_alpha,
                                                                   is_stop = token.is_stop,
                                                                   whitespace = token.whitespace_,
                                                                   ent_type = token.ent_type_.replace("'", "''"),
                                                                   is_key = not token.is_stop and not token.is_punct and not token.is_space and token.pos_ in ["NOUN", "VERB", "ADJ"])

                    # Fetch token UID
                    if token_uid is not None:
                        token_uid = token_uid[0][0]

                    token_index += 1
                    token_count += 1

                    # If a person add to character list
                    if token.ent_type_ == "PERSON":
                        potential_character = token.text.lower().replace("'", "''")
                        character_uid = database_manager.execute_fetch(select_character_uid_sql(potential_character, work_uid))
                        if character_uid is not None:
                            character_uid = character_uid[0]
                            # Add to appearance table
                            database_manager.execute(add_appearance_sql(character_uid, token_uid))
                        else:
                            database_manager.execute(add_character_sql(potential_character, work_uid))
                            character_uid = database_manager.execute_fetch(select_character_uid_sql(potential_character, work_uid))
                            if character_uid is not None:
                                character_uid = character_uid[0]
                                # Add to appearance table
                                database_manager.execute(add_appearance_sql(character_uid, token_uid))
            # If not a leaf this must be section and needs to be added to section table
            else:
                parent_uid = database_manager.execute_fetch(select_section_uid_sql(work_uid, current_node.parent.data))
                if parent_uid is not None:
                    parent_uid = parent_uid[0]
                database_manager.execute(add_section_sql(work_uid, section_count, current_node.level, parent_uid, current_node.data))
                section_count += 1          

        print(f"Added {passage_count} passages, {section_count} sections and {token_count} tokens.")

    # Finally close database connection
    database_manager.close()

    print(f"Finished in {round(time.time() - main_start_time, 3)} seconds!")

if __name__ == "__main__":
    main()
