from spacy.tokens import Span
from typing import Union

class TreeNode:
    """A node in a tree.

    Attributes:
        data (str): The data stored in this node.
        parent (TreeNode): The parent of this node.
        level (int): The level of this node in the tree.
        children (list[TreeNode]): The children of this node.
        direct_ancestors (list[TreeNode]): All ancestors of this node.
    """

    def __init__(self, data: str | Span, parent: Union['TreeNode', None]):
        self.data = data
        self.parent = parent
        self.level = 0 if self.parent is None else self.parent.level + 1
        self.children: list[TreeNode] = []
        self.direct_ancestors = [self]
        while self.direct_ancestors[-1].parent is not None:
            self.direct_ancestors.append(self.direct_ancestors[-1].parent)

    def add_child(self, child_data: str | Span) -> 'TreeNode':
        """Adds a child to this node.

        Args:
            child_data (str): The data the child node should have.

        Returns:
            TreeNode: The created child node.
        """
        self.children.append(TreeNode(child_data, self))
        return self.children[len(self.children) - 1]

    def is_leaf(self) -> bool:
        """Checks whether this node is a leaf (no children) or not.

        Returns:
            bool: `True` if a leaf, `False` if not.
        """
        return len(self.children) == 0
    
    def is_root(self) -> bool:
        """Checks whether this node is the root of the tree (no parent).

        Returns:
            bool: `True` if the root, `False` if not.
        """
        return self.parent is None

    def is_only_child(self) -> bool:
        """Checks whether this node is an only child (no siblings).

        Returns:
            bool: `True` if this node has no siblings, `False` if it has siblings. If this node is the root, `False` is returned.
        """
        if self.parent is None:
            return False
        return len(self.parent.children) == 1

    def get_child_index(self) -> int:
        """Find the index of this node in the parents children list.

        Returns:
            int: Index of this node in the parents children list. If this node is the root, -1 is returned.
        """
        if self.parent is None:
            return -1
        return self.parent.children.index(self)

    def is_first_child(self) -> bool:
        """Checks if this node is the first child of it's parent.

        Returns:
            bool: `True` if it is the first child, `False` if not.
        """
        return self.get_child_index() == 0

    def is_last_child(self) -> bool:
        """Checks if this node is the last child of it's parent.

        Returns:
            bool: `True` if it is the last child, `False` if not. If this node is the root, `False` is returned.
        """
        if self.parent is None:
            return False
        return self.get_child_index() == len(self.parent.children) - 1

    def get_ancestor_at_level(self, level_to_find: int) -> 'TreeNode':
        """Gets the ancestor of this node at the given level.

        Args:
            level_to_find (int): The level of the ancestor to find.

        Returns:
            TreeNode: The found ancestor.
        """
        return self.direct_ancestors[-level_to_find]

    def print_tree(self, level=0):
        """Prints the tree from this node downwards.

        Args:
            level (int, optional): The level of this node. Defaults to 0.
        """
        prefix = "    " * level
        print(f"{prefix}{str(self.data)}")
        for child in self.children:
            child.print_tree(level + 1)

    def __str__(self):
        return self.data

def preorder(node: TreeNode, node_list):
    """Traverses the tree in preorder and adds the nodes to the given list.

    Args:
        node (TreeNode): The node to start the traversal from.
        node_list (list[TreeNode]): The list to add the nodes to.
    """
    if node is None:
        return
    node_list.append(node)
    for n in node.children:
        preorder(n, node_list)
