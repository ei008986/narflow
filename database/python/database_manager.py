import psycopg2
from file import File
from print import print_error, print_success, print_warning
from configparser import ConfigParser
import re

SECTION = "postgresql"

class DatabaseManager():
    def __init__(self, config_file_path: str):
        # Read database config
        config_parser = ConfigParser()
        config_parser.read(config_file_path)
        if not config_parser.has_section(SECTION):
            raise Exception(f"Section [{SECTION}] is not defined in {config_file_path}.")

        # Turn database config into kwargs
        connection_kwargs = {}
        for param in config_parser.items(SECTION):
            connection_kwargs[param[0]] = param[1]

        # Try to connect to the database
        print("Attempting to connect to the database... ")
        try:
            self.database_connection = psycopg2.connect(**connection_kwargs)
            self.database_connection.autocommit = True
            print_success("Successfully connected to the database!")

            self.database_cursor = self.database_connection.cursor()

            self.print_version()
        except (Exception, psycopg2.DatabaseError) as error:
            print_error(str(error))

    def close(self):
        if self.database_connection is not None:
            self.database_cursor.close()
            self.database_connection.close()
            print_success("Database connection closed.")

    def print_version(self):
        self.database_cursor.execute("SELECT version()")
        db_version = self.database_cursor.fetchone()
        if db_version is not None:
            print(f"Database version: {db_version[0]}")

    def execute(self, command):
        self.database_cursor.execute(command)

    def execute_fetch(self, command):
        self.database_cursor.execute(command)
        try:
            return self.database_cursor.fetchone()
        except psycopg2.ProgrammingError:
            return None
    
    def execute_from_file(self, file_path: str, **kwargs):
        file = File(file_path)
        if not file.exists:
            print_error(f"File {file_path} does not exist.")
            return
        
        for key, value in kwargs.items():
            if type(value) != str:
                value = str(value)
            file.data = file.data.replace(f"${key}", value)
        
        commands = re.split("[.|^;]*(?<=[^'];)", file.data, flags=re.DOTALL)
        returners = []

        for command in commands:
            if command.strip() != "":
                returners.append(self.execute_fetch(command + ";"))
        
        return returners
    