def add_author_sql(author: str):
    return f"""DO $$
BEGIN
	IF NOT EXISTS (SELECT "Name" FROM "Author" WHERE "Name" = '{author}') THEN
		INSERT INTO "Author" ("Name") VALUES ('{author}');
	END IF;
END $$"""

def add_work_sql(title, author_uid, version, blurb, genre, date, work_type, image_uid):
	return f""" INSERT INTO "Work"("Title", "AuthorUID", "Version", "Blurb", "Type", "Genre", "Date", "ImageUID")
			    VALUES 
				('{title}',
				{author_uid},
				'{version}',
				'{blurb}',
				'{work_type}',
				'{genre}',
				'{date}',
				'{image_uid}'
				);""".replace("'None'", "null")

def delete_work_sql(work_uid):
	return f"""DELETE FROM "Work" WHERE "WorkUID" = {work_uid};"""

def add_image_sql(url, attribution):
	return f"""INSERT INTO "Image"("URL", "Attribution") VALUES ('{url}', '{attribution}');""".replace("'None'", "null")

def select_image_uid_sql(url):
	return f"""SELECT "ImageUID" FROM "Image" WHERE "URL" = '{url}'"""

def add_section_sql(work_uid, index, level, parent_uid, title):
	if parent_uid is None:
		parent_uid = "null"
	return f"""INSERT INTO "Section"("WorkUID", "Index", "Level", "ParentUID", "Title") VALUES ({work_uid}, {index}, {level}, {parent_uid}, '{title}');"""

def add_passage_sql(work_uid, section_uid, index, text, narflow_score):
	if section_uid is None:
		section_uid = "null"
	return f"""INSERT INTO "Passage"("WorkUID", "SectionUID", "Index", "Text", "NarflowScore") VALUES ({work_uid}, {section_uid}, {index}, '{text}', {narflow_score if narflow_score is not None else "null"});"""

def add_dialogue_sql(character_uid, passage_uid, range):
	return f"""INSERT INTO "Dialogue"("CharacterUID", "PassageUID", "Range") VALUES ({character_uid}, {passage_uid}, {range});"""

def select_work_sql(title):
	return f"""SELECT "WorkUID", "Version" FROM "Work" WHERE "Title" = '{title}'"""

def select_author_uid_sql(author):
	return f"""SELECT "AuthorUID" FROM "Author" WHERE "Name" = '{author}'"""

def select_work_uid_sql(title):
	return f"""SELECT "WorkUID" FROM "Work" WHERE "Title" = '{title}'"""

def select_section_uid_sql(work_uid, title):
	return f"""SELECT "SectionUID" FROM "Section" WHERE "WorkUID" = {work_uid} AND "Title" = '{title}'"""

def select_passage_uid_sql(work_uid, index):
	return f"""SELECT "PassageUID" FROM "Passage" WHERE "WorkUID" = {work_uid} AND "Index" = {index}"""

def add_token_sql(passage_uid, index, text, lemma, simple_pos, detailed_pos, semantic_dependancy, shape, is_alpha, is_stop, whitespace):
	return (f"""
		INSERT INTO "Token" (
			"PassageUID",
			"Index",
			"Text",
			"Lemma",
			"SimplePOS",
			"DetailedPOS",
			"SemanticDependancy",
			"Shape",
			"Alpha",
			"Stop",
			"Whitespace"
		)
		VALUES (
			{passage_uid},
			{index},
			'{text}',
			'{lemma}',
			'{simple_pos}',
			'{detailed_pos}',
			'{semantic_dependancy}',
			'{shape}',
			{is_alpha},
			{is_stop},
			'{whitespace}'
		)
		RETURNING "TokenUID";
		"""
	)

def add_character_sql(name, work_uid):
	return f"""INSERT INTO "Character" ("Name", "WorkUID") VALUES ('{name}', {work_uid});"""

def select_character_uid_sql(name, work_uid):
	return f"""SELECT "CharacterUID" FROM "Character" WHERE "Name" = '{name}' AND "WorkUID" = {work_uid};"""

def add_appearance_sql(character_uid, token_uid):
	return f"""INSERT INTO "Appearance" ("CharacterUID", "TokenUID") VALUES ({character_uid}, {token_uid});"""

def select_token_uid_sql(passage_uid, index):
	return f"""SELECT "TokenUID" FROM "Token" WHERE "PassageUID" = {passage_uid} AND "Index" = {index};"""