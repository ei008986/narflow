from spacy.tokens import Span

def calculate_narrative_flow_score(text: Span, next_text: Span, previous_text: Span) -> float:
    # Calculate the difference in similarity between the current text and the next text
    previous_similarity = text.similarity(previous_text)
    return previous_similarity