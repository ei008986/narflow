import spacy

# load Spacy's pre-trained model
nlp = spacy.load("en_core_web_sm")

def is_keyword(token):
    """
    Determine if a Spacy token is a keyword.
    """
    return token.pos_ in ["NOUN", "VERB", "ADJ"]

# define the text to be analyzed
text = "The quick brown fox jumped over the lazy dog."

# pass the text through Spacy's pipeline to get a processed Doc object
doc = nlp(text)

# extract the key words from the processed Doc object
keywords = set()
for token in doc:
    if is_keyword(token):
        keywords.add(token.lemma_)

# print the identified key words
print(keywords)
