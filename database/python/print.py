# ANSI escape sequences for console colors
# Credit to: https://svn.blender.org/svnroot/bf-blender/trunk/blender/build_files/scons/tools/bcolors.py
SUCCESS_COLOR = '\033[92m'
WARNING_COLOR = '\033[93m'
ERROR_COLOR = '\033[91m'
END_COLOR = '\033[0m'

def print_error(message: str):
    """Prints a message in red.

    Args:
        message (str): The message to print.
    """
    print(f"{ERROR_COLOR}{message}{END_COLOR}")

def print_warning(message: str):
    """Prints a message in yellow.

    Args:
        message (str): The message to print.
    """
    print(f"{WARNING_COLOR}{message}{END_COLOR}")

def print_success(message: str):
    """Prints a message in green.

    Args:
        message (str): The message to print.
    """
    print(f"{SUCCESS_COLOR}{message}{END_COLOR}")