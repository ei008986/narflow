DROP TABLE IF EXISTS "Author" CASCADE;
DROP TABLE IF EXISTS "Passage" CASCADE;
DROP TABLE IF EXISTS "Section" CASCADE;
DROP TABLE IF EXISTS "Work" CASCADE;
DROP TABLE IF EXISTS "Appearance" CASCADE;
DROP TABLE IF EXISTS "Character" CASCADE;
DROP TABLE IF EXISTS "Dialogue" CASCADE;
DROP TABLE IF EXISTS "Image" CASCADE;
DROP TABLE IF EXISTS "Token" CASCADE;