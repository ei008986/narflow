INSERT INTO "Token" 
(
    "PassageUID",
    "WorkUID",
    "Index",
    "Text",
    "Lemma",
    "SimplePOS",
    "DetailedPOS",
    "SemanticDependancy",
    "Shape",
    "Alpha",
    "Stop",
    "Whitespace",
    "EntType",
    "Key"
)
VALUES (
    $passage_uid,
    $work_uid,
    $index,
    '$text',
    '$lemma',
    '$simple_pos',
    '$detailed_pos',
    '$semantic_dependancy',
    '$shape',
    $is_alpha,
    $is_stop,
    '$whitespace',
    '$ent_type',
    $is_key
)
RETURNING "TokenUID";
