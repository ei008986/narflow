-- Create Image table

CREATE TABLE IF NOT EXISTS "Image"
(
    "ImageUID" integer NOT NULL GENERATED ALWAYS AS IDENTITY,
    "URL" text NOT NULL,
    "Attribution" text,
    CONSTRAINT "PK_Image" PRIMARY KEY ("ImageUID")
);

-- Create Author table

CREATE TABLE IF NOT EXISTS "Author"
(
    "AuthorUID" integer NOT NULL GENERATED ALWAYS AS IDENTITY,
    "Name" text NOT NULL,
    "ImageUID" integer,
    CONSTRAINT "PK_Author" PRIMARY KEY ("AuthorUID"),
    CONSTRAINT "FK_Image" FOREIGN KEY ("ImageUID") REFERENCES "Image" ("ImageUID")
);

-- Create Work table

CREATE TABLE IF NOT EXISTS "Work"
(   
    "WorkUID" integer NOT NULL GENERATED ALWAYS AS IDENTITY,
    "Title" text NOT NULL,
    "AuthorUID" integer NOT NULL,
    "Blurb" text,
    "Version" text,
    "Type" text,
    "Genre" text,
    "Date" text,
    "ImageUID" integer,
    CONSTRAINT "PK_Work" PRIMARY KEY ("WorkUID"),
    CONSTRAINT "FK_Author" FOREIGN KEY ("AuthorUID") REFERENCES "Author" ("AuthorUID"),
    CONSTRAINT "FK_Image" FOREIGN KEY ("ImageUID") REFERENCES "Image" ("ImageUID")
);

-- Create Character table

CREATE TABLE IF NOT EXISTS "Character"
(
    "CharacterUID" integer NOT NULL GENERATED ALWAYS AS IDENTITY,
    "Name" text NOT NULL,
    "WorkUID" integer NOT NULL,
    "Description" text,
    CONSTRAINT "PK_Character" PRIMARY KEY ("CharacterUID")
);

-- Create Section table

CREATE TABLE IF NOT EXISTS "Section"
(
    "SectionUID" integer NOT NULL GENERATED ALWAYS AS IDENTITY,
    "WorkUID" integer NOT NULL,
    "Index" integer NOT NULL,
    "Level" integer NOT NULL,
    "ParentUID" integer,
    "Title" text,
    CONSTRAINT "PK_Section" PRIMARY KEY ("SectionUID"),
    CONSTRAINT "FK_Work" FOREIGN KEY ("WorkUID") REFERENCES "Work" ("WorkUID") ON DELETE CASCADE
);

-- Create Passage table

CREATE TABLE IF NOT EXISTS "Passage"
(
    "PassageUID" integer NOT NULL GENERATED ALWAYS AS IDENTITY,
    "WorkUID" integer NOT NULL,
    "SectionUID" integer,
    "Index" integer NOT NULL,
    "NarflowScore" float,
    "Text" text,
    CONSTRAINT "PK_Passage" PRIMARY KEY ("PassageUID"),
    CONSTRAINT "FK_Work" FOREIGN KEY ("WorkUID") REFERENCES "Work" ("WorkUID") ON DELETE CASCADE,
    CONSTRAINT "FK_Section" FOREIGN KEY ("SectionUID") REFERENCES "Section" ("SectionUID")
);

-- Create Token table

CREATE TABLE IF NOT EXISTS "Token"
(
    "TokenUID" integer NOT NULL GENERATED ALWAYS AS IDENTITY,
    "PassageUID" integer NOT NULL,
    "WorkUID" integer NOT NULL,
    "Index" integer NOT NULL,
    "Text" text NOT NULL,
    "Lemma" text,
    "SimplePOS" text,
    "DetailedPOS" text,
    "SemanticDependancy" text,
    "Shape" text,
    "Alpha" boolean,
    "Stop" boolean,
    "Whitespace" text,
    "EntType" text,
    "Key" boolean,
    CONSTRAINT "PK_Token" PRIMARY KEY ("TokenUID")
);

-- Create Appearance (join) table

CREATE TABLE IF NOT EXISTS "Appearance"
(
    "CharacterUID" integer NOT NULL,
    "TokenUID" integer NOT NULL,
    CONSTRAINT "FK_Character" FOREIGN KEY ("CharacterUID") REFERENCES "Character" ("CharacterUID"),
    CONSTRAINT "FK_Token" FOREIGN KEY ("TokenUID") REFERENCES "Token" ("TokenUID")
);

