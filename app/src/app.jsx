import React from 'react';
import Navbar from './components/navbar';
import Compendium from './pages/compendium';
import Corpus from './pages/corpus';
import View from './pages/view';
import Info from './pages/info';
import Help from './pages/help';
import Home from './pages/home';
import Input from './pages/input';
import { Route, Routes } from 'react-router-dom';

function App()
{
    return (
        <React.Fragment>
            <header>
                <Navbar />
            </header>
            <main className='flex-grow flex-vertical'>
                <Routes>
                    <Route path="/" element={<Home />}/>
                    <Route path="/compendium" element={<Compendium />}/>
                    <Route path="/corpus" element={<Corpus />}/>
                    <Route path="/view" element={<View />}/>
                    <Route path="/info" element={<Info />}/>
                    <Route path="/help" element={<Help />}/>
                    <Route path="/input" element={<Input />}/>
                </Routes>
            </main>
        </React.Fragment>
    )
}

export default App;