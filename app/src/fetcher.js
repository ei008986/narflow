import { useState, useEffect } from 'react';

export default function useFetchData(request) 
{
    // State and setter for data
    const [data, setData] = useState([]);
    
    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await fetch(`http://localhost:5000/${request}`);
                const jsonData = await response.json();
                setData(jsonData);
            } catch (error) {
                console.error(error.message);
            }
        };
        
        fetchData(); 
    }, [request]); // Only re-run the effect if request changes
    
    return data;
}
