import React from 'react';
import useFetchData from '../fetcher';
import {useNavigate} from 'react-router-dom';
import Stat from '../components/stat';

function Info() {

    const viewParams = (new URL(document.location)).searchParams;
    const title = viewParams.get("w").replace(/-/g, ' ');
    
    const navigate = useNavigate();
    const goToView = () => { navigate("/view?w=" + title.replace(/\s+/g, '-')); }
    
    const info = useFetchData(`info?title=${title}`)
    const tokenCount = useFetchData(`token-frequency?work_id=${info.WorkUID}`).count
    const characterCount = useFetchData(`appearance?work_id=${info.WorkUID}`).length
    const passageCount = useFetchData(`passage?id=${info.WorkUID}`).length
    const sectionCount = useFetchData(`section?id=${info.WorkUID}`).length
    const wordCount = useFetchData(`word-count?work_id=${info.WorkUID}`).count
    const stopWordCount = useFetchData(`stop-word-count?work_id=${info.WorkUID}`).count
    const uniqueWordCount = useFetchData(`unique-word-count?work_id=${info.WorkUID}`).count
    const uniqueCharCount = useFetchData(`unique-character-count?work_id=${info.WorkUID}`).count

    return (
        <div className="layout-container flex-grow">
            <div id="l-layout" className='sticky-grid'>
                <h1 className='work-title'>{info.Title}</h1>
                <p className='author-name'>{info.Author}</p>
                <p className='blurb'>{info.Blurb}</p>
                <div className='info-box'>
                    <p className='type'>Type: {info.Type}</p>
                    <p className='genre'>Genre: {info.Genre}</p>
                </div>
                <button className="important mt-10" onClick={goToView}>
                    <span className="material-symbols-outlined icon">auto_stories</span>
                    View
                </button>
            </div>
            <div id="c-layout flex-grow">
                <div className="stat-grid flex-grow">
                    <Stat title={"Section Count"} stat={sectionCount} />
                    <Stat title={"Passage Count"} stat={passageCount} />
                    <Stat title={"Token Count"} stat={tokenCount} />
                    <Stat title={"Word Count"} stat={wordCount} />
                    <Stat title={"Stopword Count"} stat={stopWordCount} />
                    <Stat title={"Unique Words"} stat={uniqueWordCount} />
                    <Stat title={"Character Count"} stat={uniqueCharCount} />
                    <Stat title={"Character Appearances"} stat={characterCount} />
                    <Stat title={"Release Date"} stat={info.Date} />
                </div>
            </div>
            <div id="r-layout">
                <img src={info.ImageURL} className='image' alt={`Picture representing ${title}.`}/>
                <p>{info.ImageAttribution}</p>
            </div>
        </div>
    )
}


export default Info;