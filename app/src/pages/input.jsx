import React, { Fragment } from 'react';

export default function Input() {
    const input_field = React.createRef();

    const clear = () => { input_field.current.value = ''; }

    const paste = () => { 
        navigator.clipboard.readText().then(text => input_field.current.value = text); 
    }

    return (
        <div className='center-column flex-vertical flex-grow'>
            <h1>Custom Input</h1>
            <p>Enter text to analyse</p>
            <textarea className='input flex-grow' ref={input_field}/>
            <div className='flex-horizontal space-children'>
                <button className='important'>View</button>
                <button onClick={clear}>Clear</button>
                <button onClick={paste}>Paste</button>
            </div>
        </div>
    )
}