import React from 'react';
import Stat from '../components/stat';
import useFetchData from '../fetcher';

function Compendium() {
    let workCount = useFetchData(`info`).length
    let tokenCount = useFetchData(`token-frequency`).count
    let characterCount = useFetchData(`appearance`).length
    let passageCount = useFetchData(`passage`).length
    let sectionCount = useFetchData(`section`).length
    let wordCount = useFetchData(`word-count`).count
    let stopWordCount = useFetchData(`stop-word-count`).count
    let uniqueWordCount = useFetchData(`unique-word-count`).count
    let uniqueCharCount = useFetchData(`unique-character-count`).count


    return (
        <div className="layout-container flex-grow">
            <div id="l-layout" className='sticky-grid'>
                <h1>Compendium</h1>
                <p>Collection of statistics about the corpus as a whole.</p>
            </div>
            <div id="c-layout flex-grow">
                <div className="stat-grid flex-grow">
                    <Stat title={"Section Count"} stat={sectionCount} />
                    <Stat title={"Passage Count"} stat={passageCount} />
                    <Stat title={"Token Count"} stat={tokenCount} />
                    <Stat title={"Word Count"} stat={wordCount} />
                    <Stat title={"Stopword Count"} stat={stopWordCount} />
                    <Stat title={"Unique Words"} stat={uniqueWordCount} />
                    <Stat title={"Character Count"} stat={uniqueCharCount} />
                    <Stat title={"Character Appearances"} stat={characterCount} />
                    <Stat title={"Works in Corpus"} stat={workCount} />
                </div>
            </div>
            <div id="r-layout">
            </div>
        </div>
    )
}

export default Compendium;