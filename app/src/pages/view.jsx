import React from 'react';
import Contents from '../components/contents';
import useFetchData from '../fetcher';
import Section from '../components/section';
import {useNavigate} from 'react-router-dom';

export default function View() {
    const viewParams = (new URL(document.location)).searchParams;
    const title = viewParams.get("w").replace(/-/g, ' ');

    const info = useFetchData(`info?title=${title}`)
    const sections = useFetchData(`section?title=${title}&level=1`) // Level 1 only for now

    const allowCollapse = info.Type === "prose" || info.Type === "play";
    const showTitle = sections.length > 1 && allowCollapse;

    const navigate = useNavigate();

    let mode = viewParams.get("mode");
    const baseURL = "/view?w=" + title.replace(/\s+/g, '-');

    function swapToMode(newMode) {
        if (mode === newMode) {
            navigate(baseURL);
            mode = viewParams.get("mode");
        }

        else {
            navigate(baseURL + "&mode=" + newMode);
            mode = viewParams.get("mode");
        }
    }

    const modeNarrativeFlow = () => swapToMode("narrative-flow");
    const modeCharacters = () => swapToMode("characters");
    const modeKeywords = () => swapToMode("keywords");
    const modeStop = () => swapToMode("stop");
    const modeNoun = () => swapToMode("noun");
    const modeVerb = () => swapToMode("verb");
    const modeAdj = () => swapToMode("adj");
    const modeAdv = () => swapToMode("adv");

    return (
        <div className="layout-container">
            <div id="l-layout" className='sticky-grid'>
                <h1 className='work-title'>{info.Title}</h1>
                <p className='author-name'>{info.Author}</p>
                <p className='blurb'>{info.blurb}</p>
                <Contents title={title} shouldDisplay={sections.length > 1}/>
            </div>
            <div id="c-layout">
                {sections.map(section => ( <Section key={section.Index} section={section} showTitle={showTitle} allowCollapse={allowCollapse}/> ))}
            </div>
            <div id="r-layout" className='lenses'>
                <button onClick={modeNarrativeFlow}>Narrative Flow</button>
                <button onClick={modeCharacters}>Characters</button>
                <button onClick={modeKeywords}>Keywords</button>
                <button onClick={modeStop}>Stop Words</button>
                <button onClick={modeNoun}>Nouns</button>
                <button onClick={modeVerb}>Verbs</button>
                <button onClick={modeAdj}>Adjectives</button>
                <button onClick={modeAdv}>Adverbs</button>
            </div>
        </div>
    )
}
