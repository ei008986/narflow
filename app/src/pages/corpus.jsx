import React, { useEffect, useState } from 'react';
import WorkThumbnail from '../components/work-thumbnail';
import { Link } from 'react-router-dom';

function Corpus() {

    const [works, setWorks] = useState([]) 

    const getWorks = async () => {
        try {
            const response = await fetch("http://localhost:5000/info")
            const jsonData = await response.json()
            setWorks(jsonData)
        } catch (error) {
            console.error(error.message)
        }
    }

    useEffect(() => {
        getWorks();
    }, []);

    return (
        <div className="center-column mt-20">
            <div className='page-title'>
                <h1>Corpus</h1>
                <p>Collection of poems, plays and prose</p>
            </div>
            <div className='flex-vertical thumbnail-gap'>
                {works.map(work => (
                    <WorkThumbnail key={work.WorkUID} title={work.Title} author={work.Author}/>
                ))}
                <div className='note'>
                    <p>Narflow isn't limited to analyzing literary works; you can upload or paste your own text <Link to="/input">here</Link>.</p>
                </div>
            </div>
        </div>
    )
}

export default Corpus;