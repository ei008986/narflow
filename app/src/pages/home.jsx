import React from 'react';
import { useNavigate } from "react-router-dom";

export default function Home() 
{
    const navigate = useNavigate();
    const goToApp = () => { navigate("/input"); }
    const goToCorpus = () => { navigate("/corpus"); }

    return (
        <div className="flex-grow flex-vertical flex-center waves primary-background">
            <h1 className='tagline fancy-font'>Narflow</h1>
            <span>Narrative Flow Analysis System</span>
            <div className="flex-horizontal">
                <button className="important space" onClick={goToCorpus}>
                    <span className="material-symbols-outlined icon">auto_stories</span>
                    View Corpus
                </button>
                <button className="space" onClick={goToApp}>
                    <span className="material-symbols-outlined icon">edit</span>
                    Input Text
                </button>
            </div>
        </div>
    )
}
