import React, { Fragment } from 'react';

function Help() {
    return (
        <div className='center-column flex-vertical flex-grow helper'> 
            <h1>Help</h1>
            <h2>How to analyse my own text?</h2>
            <p>Open the "Custom Input" page and just paste your text in the text area and click on the button "Analyse".</p>
            <h2>How to analyse other texts?</h2>
            <p>Open the "Corpus" page and click the "View" button next to the work you want to analyse.</p>
            <h2>How to access different analysis modes?</h2>
            <p>On the view page, click on the buttons on the right to change modes. Only one mode can be applied at one time.</p>
            <h2>Can I add works to the corpus?</h2>
            <p>No.</p>
            <h2>Can I delete/edit works from the corpus?</h2>
            <p>No.</p>
            <h2>How can I view statistics and info about a work?</h2>
            <p>Open the "Corpus" page and click the "Info" button next to the work you want to see information about.</p>
            <h2>How can I view statistics and info about the corpus as a whole?</h2>
            <p>Open the "Compendium" page.</p>
        </div>
    );
}

export default Help;