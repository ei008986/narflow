export default function Stat({title, stat})
{
    return (
        <div className='stat-box flex-vertical flex-center'>
            <p>{title}</p>
            <h2>{stat}</h2>
        </div>
    )
}
