import React from "react"
import useFetchData from "../fetcher"
import Passage from "./passage"

export default function Section({section, showTitle, allowCollapse})
{
    // Get passages in this section
    const passages = useFetchData(`passage?id=${section.WorkUID}&section_index=${section.Index}&section_level=${section.Level}`)
    const mappedPassages = passages.map(p => ( <Passage key={p.Index} passage={p}/> ))

    // Get subsections in this section
    const subsections = useFetchData(`section?id=${section.WorkUID}&parent_id=${section.SectionUID}&level=${section.Level + 1}`)
    const mappedSubsections = subsections.map(subsection => ( <Section key={subsection.Index} section={subsection} showTitle={true} allowCollapse={allowCollapse} /> ))

    // Toggle content
    let content = React.createRef()
    let button = React.createRef()
    let container = React.createRef()

    function toggleContent()
    {
        if (allowCollapse == false) return;
        content.current.classList.toggle("hidden")
        button.current.classList.toggle("twist")
        container.current.classList.toggle("section-shadow")
    }

    const id = `${section.SectionUID}`

    return (
        <div className="section flex-vertical flex-shrink" ref={container} id={id}>            
            {showTitle || allowCollapse ? React.createElement(`h${section.Level + 1}`, {className: "section-title"}, section.Title) : null}
            {allowCollapse ?
            <button className="section-toggle" onClick={toggleContent}>
                <span className="material-symbols-outlined" ref={button}>expand_less</span>
            </button>
            : null}
            <div className="section-content" ref={content}>
                {mappedPassages}
                {mappedSubsections}
            </div>
        </div>
    )
}
