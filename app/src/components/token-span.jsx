import React, { Fragment } from "react";
import useFetchData from "../fetcher";
import Token from "./token";

export default function Passage({passage})
{
    const tokens = useFetchData(`token?id=${passage.PassageUID}`)
    const mappedTokens = tokens.map(t => ( <Token key={t.TokenUID} token={t} /> ))

    return mappedTokens
}