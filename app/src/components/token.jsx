import React, { useState } from 'react';
import useFetchData from "../fetcher";

export default function Token({token})
{
    const tooltip = React.createRef();
    const word = React.createRef();

    const character = useFetchData(`appearance?token_id=${token.ID}`)
    const lemmaFrequency = useFetchData(`token-frequency?lemma=${token.Lemma}`)
    const textFrequency = useFetchData(`token-frequency?text=${token.Text}`)

    const viewParams = (new URL(document.location)).searchParams;
    let mode = viewParams.get("mode");

    let keywordHighlight = mode === "keywords" && token.Key === true;
    let characterHighlight = mode === "characters" && token.EntType === 'PERSON';
    let stopHighlight = mode === "stop" && token.Stop === true;
    let nounHighlight = mode === "noun" && token.SimplePOS === 'NOUN';
    let verbHighlight = mode === "verb" && token.SimplePOS === 'VERB';
    let adjHighlight = mode === "adj" && token.SimplePOS === 'ADJ';
    let advHighlight = mode === "adv" && token.SimplePOS === 'ADV';

    const showTooltip = () => {
        tooltip.current.classList.remove('token-hidden');
        word.current.classList.add('token-highlight');
    }

    const hideTooltip = () => {
        tooltip.current.classList.add('token-hidden');
        word.current.classList.remove('token-highlight');
    }

    return (
        <span onMouseEnter={showTooltip} onMouseLeave={hideTooltip}>
            <span ref={word} className={`token
                                        ${keywordHighlight ? 'token-highlight-keyword' : ''}
                                        ${characterHighlight ? 'token-highlight-character' : ''}
                                        ${stopHighlight ? 'token-highlight-stop' : ''}
                                        ${nounHighlight ? 'token-highlight-noun' : ''}
                                        ${verbHighlight ? 'token-highlight-verb' : ''}
                                        ${adjHighlight ? 'token-highlight-adj' : ''}
                                        ${advHighlight ? 'token-highlight-adv' : ''}
                                        `}>
                {token.Text}
            </span>
            <span>{token.Whitespace}</span>
            <span className='token-tooltip token-hidden' ref={tooltip}>
                <div><b>Text:</b> {token.Text}</div>
                <div><b>Lemma:</b> {token.Lemma}</div>
                <div><b>POS:</b> {token.SimplePOS}</div>
                <div><b>Detailed POS:</b> {token.DetailedPOS}</div>
                <div><b>Semantic Dependancy:</b> {token.SemanticDependancy}</div>
                <div><b>Shape:</b> {token.Shape}</div>
                <div><b>Is Alpha:</b> {token.Alpha.toString()}</div>
                <div><b>Is Stop:</b> {token.Stop.toString()}</div>
                <div><b>Ent Type:</b> {token.EntType}</div>
                <div><b>Character:</b> {character.Name}</div>
                <div><b>Is Key:</b> {token.Key.toString()}</div>
                <div><b>Lemma Frequency (in corpus):</b> {lemmaFrequency.count}</div>
                <div><b>Text Frequency (in corpus):</b> {textFrequency.count}</div>
            </span>
        </span>
    )
}