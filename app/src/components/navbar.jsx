import React from 'react';
import { Link, useMatch, useResolvedPath } from 'react-router-dom';

export default function Navbar() {
    return (
        <React.Fragment>
            <Link to="/" id="header-logo">Narflow</Link>
            <nav className="align-right">
                <ul>
                    <NavLink to="/corpus">Corpus</NavLink>
                    <NavLink to="/input">Custom Input</NavLink>
                    <NavLink to="/compendium">Compendium</NavLink>
                    <NavLink to="/help">Help</NavLink>
                </ul>
            </nav>
        </React.Fragment>
    )
}

function NavLink({to, children, ...props}) {
    const resolvedPath = useResolvedPath(to)
    const isActive = useMatch({ path: resolvedPath.pathname, end: true})
    return <li className={isActive ? "active" : ""}><Link to={to} {...props}>{children}</Link></li>
}
