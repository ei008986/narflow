import React, { Fragment } from 'react';
import {useNavigate} from 'react-router-dom';

export default function WorkThumbnail({title, author}) {
    const navigate = useNavigate();

    const page = title.replace(/\s+/g, '-')

    function goToView()
    {
        navigate("/view?w=" + page);
    }  

    function goToInfo()
    {
        navigate("/info?w=" + page);
    }


    return (
        <Fragment>
            <div className='flex-horizontal thumbnail'>
                <div className='flex-vertical'>
                    <h2 className='work-title'>{title}</h2>
                    <p className='author-name'>{author}</p>
                </div>
                <div className='flex-horizontal align-right button-gap'>
                    <button className='important' onClick={goToView}>
                        <span className="material-symbols-outlined">frame_inspect</span>
                        View
                    </button>
                    <button onClick={goToInfo}>
                        <span className="material-symbols-outlined">query_stats</span>
                        Info
                    </button>
                </div>
            </div>
        </Fragment>
    )
}