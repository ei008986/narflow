import React, { Fragment } from "react";
import ReactDOM from "react-dom";
import createRoot from "react-dom/client";
import { useState } from "react";
import TokenSpan from "./token-span";

export default function Passage({passage})
{
    const container = React.createRef();
    const text = React.createRef();
    const [isHovered, setIsHovered] = useState(false);

    const showTokens = () => {
        setIsHovered(true);
        text.current.style.display = "none";
    };

    const viewParams = (new URL(document.location)).searchParams;
    let mode = viewParams.get("mode");
    let narflowHighlight = mode === "narrative-flow";

   
    return (
        <Fragment>
            <div ref={container} onMouseEnter={showTokens} className={`passage-container
                                                                        ${narflowHighlight && passage.NarflowScore < 0 ? 'narflow-neg' : ''}
                                                                        ${narflowHighlight && passage.NarflowScore === 0 ? 'narflow-neutral' : ''}
                                                                        ${narflowHighlight && passage.NarflowScore > 0 && passage.NarflowScore < 0.25 ? 'narflow-pos1' : ''}
                                                                        ${narflowHighlight && passage.NarflowScore >= 0.25 && passage.NarflowScore < 0.5 ? 'narflow-pos2' : ''}
                                                                        ${narflowHighlight && passage.NarflowScore >= 0.5 && passage.NarflowScore < 0.75 ? 'narflow-pos3' : ''}
                                                                        ${narflowHighlight && passage.NarflowScore >= 0.75 ? 'narflow-pos4' : ''}
                                                                      `}>
                <span ref={text}>{passage.Text}</span>
                {isHovered && <TokenSpan passage={passage}/>}
            </div>
        </Fragment>
    )
}