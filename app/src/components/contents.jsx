import React, { Fragment } from 'react'
import useFetchData from '../fetcher'
import { Link } from 'react-router-dom'

export default function Contents({title, shouldDisplay})
{
    const sections = useFetchData(`section?title=${title}`) // Level 1 only for now

    // If we shouldn't display just return null
    if (!shouldDisplay)
    {
        return null
    }

    function scrollToElement(id) {
        const element = document.getElementById(id);
        if (element) {
          element.scrollIntoView({ behavior: 'smooth' });
        }
      }

    // Otherwise return the contents panel
    return (
        <Fragment>
            <hr />
            <h2>Contents</h2>
            {sections.map(s => ( 
                <p onClick={() => scrollToElement(s.SectionUID)} className='contents-section' key={s.SectionUID}>{s.Title}</p>
            ))}
        </Fragment>
    )
}