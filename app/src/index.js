import React from "react";
import "./styles.css"
import App from "./app"
import {BrowserRouter} from "react-router-dom"
import * as ReactDOMClient from 'react-dom/client';

const root = ReactDOMClient.createRoot(document.getElementById("root"))
root.render(
    <BrowserRouter>
        <App />
    </BrowserRouter>
)