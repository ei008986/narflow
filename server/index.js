const express = require("express");
const app = express();
const cors = require("cors");
const pool = require("./db")

// Middleware
app.use(cors());
app.use(express.json());

// Get one work by title
app.get("/work/:title", async (req, res) => {
    try {
        const {title} = req.params;
        const allWorks = await pool.query(`SELECT * FROM "Work" WHERE "Title" = '${title}';`);
        res.json(allWorks.rows[0]);
    } catch (error) {
        console.error(error.message);
    }
});

// Get one work by id
app.get("/work/id/:id", async (req, res) => {
    try {
        const {id} = req.params;
        const allWorks = await pool.query(`SELECT "Work"."Title", "Author"."Name" FROM "Work" INNER JOIN "Author" ON "Work"."AuthorUID"="Author"."AuthorUID" WHERE "WorkUID" = ${id}`);
        res.json(allWorks.rows[0]);
    } catch (error) {
        console.error(error.message);
    }
});

// Get author by id
app.get("/author/id/:id", async (req, res) => {
    try {
        const {id} = req.params;
        const allAuthors = await pool.query(`SELECT * FROM \"Author\" WHERE \"AuthorUID\" = ${id};`);
        res.json(allAuthors.rows[0]);
    } catch (error) {
        console.error(error.message);
    }
});

// Get author by name
app.get("/author/:name", async (req, res) => {
    try {
        const {name} = req.params;
        const allAuthors = await pool.query(`SELECT * FROM \"Author\" WHERE \"Name\" = '${name}';`);
        res.json(allAuthors.rows[0]);
    } catch (error) {
        console.error(error.message);
    }
});

// GET INFO
app.get("/info", async (req, res) => {
    try {
        let query = `SELECT w."WorkUID", w."Title", a."Name" AS "Author", w."Blurb", w."Type", w."Genre", w."Date", i."URL" as "ImageURL", i."Attribution" as "ImageAttribution"
                     FROM "Work" w 
                     INNER JOIN "Author" a ON w."AuthorUID" = a."AuthorUID"
                     INNER JOIN "Image" i ON w."ImageUID" = i."ImageUID"`;
        const id = req.query.id;
        const title = req.query.title;
        
        if (id && title) {
            const escapedTitle = title.replace(/'/g, "''");
            query += ` WHERE w."WorkUID" = ${id} OR w."Title" ILIKE '%${escapedTitle}%'`;
        } else if (id) {
            query += ` WHERE w."WorkUID" = ${id}`;
        } else if (title) {
            const escapedTitle = title.replace(/'/g, "''");
            query += ` WHERE w."Title" ILIKE '%${escapedTitle}%'`;
        }
        
        const allWorks = await pool.query(query);
        const result = allWorks.rows;
        
        // If only one result, return object instead of array
        if (result.length === 1) {
            res.json(result[0]);
        } else {
            res.json(result);
        }
    } catch (error) {
        console.error(error.message);
    }
});

// GET SECTIONS
app.get("/section", async (req, res) => {
    try {
        let query = `SELECT w."WorkUID", s."SectionUID", s."Index", s."Level", s."Title" FROM "Section" s JOIN "Work" w ON w."WorkUID" = s."WorkUID"`;
        const id = req.query.id;
        const title = req.query.title;
        const index = req.query.index;
        const level = req.query.level;
        const parent_id = req.query.parent_id;

        // Add id and title to query
        if (id && title) {
            const escapedTitle = title.replace(/'/g, "''");
            query += ` WHERE w."WorkUID" = ${id} OR w."Title" ILIKE '%${escapedTitle}%'`;
        } else if (id) {
            query += ` WHERE w."WorkUID" = ${id}`;
        } else if (title) {
            const escapedTitle = title.replace(/'/g, "''");
            query += ` WHERE w."Title" ILIKE '%${escapedTitle}%'`;
        }

        // Add index and level to query
        if (index && level) {
            query += ` AND s."Index" = ${index} AND s."Level" = ${level}`;
        } else if (index) {
            query += ` AND s."Index" = ${index}`;
        } else if (level) {
            query += ` AND s."Level" = ${level}`;
        }

        // Add parent id to query
        if (parent_id) {
            query += ` AND s."ParentUID" = ${parent_id}`;
        }

        const allSections = await pool.query(query);

        res.json(allSections.rows);
    } catch (error) {
        console.error(error.message);
    }
});

// GET PASSAGES
app.get("/passage", async (req, res) => {
    try {
        let query = `SELECT p."PassageUID", p."Index", p."Text", s."Index" as "SectionIndex", s."Level" as "SectionLevel", p."NarflowScore" FROM "Passage" p JOIN "Work" w ON w."WorkUID" = p."WorkUID" JOIN "Section" s ON p."SectionUID" = s."SectionUID"`;
        const id = req.query.id;
        const title = req.query.title;
        const section_index = req.query.section_index;
        const section_level = req.query.section_level;

        // Add id and title to query
        if (id && title) {
            const escapedTitle = title.replace(/'/g, "''");
            query += ` WHERE w."WorkUID" = ${id} OR w."Title" ILIKE '%${escapedTitle}%'`;
        } else if (id) {
            query += ` WHERE w."WorkUID" = ${id}`;
        } else if (title) {
            const escapedTitle = title.replace(/'/g, "''");
            query += ` WHERE w."Title" ILIKE '%${escapedTitle}%'`;
        }

        // Add section index and level to query
        if (section_index && section_level) {
            query += ` AND s."Index" = ${section_index} AND s."Level" = ${section_level}`;
        } else if (section_index) {
            query += ` AND s."Index" = ${section_index}`;
        } else if (section_level) {
            query += ` AND s."Level" = ${section_level}`;
        }

        const allSections = await pool.query(query);
        res.json(allSections.rows);
    } catch (error) {
        console.error(error.message);
    }
});

// GET TOKENS
app.get("/token", async (req, res) => {
    try {
        let query = `SELECT * FROM "Token"`;
        const id = req.query.id;

        // Add id to query
        if (id) {
            query += ` WHERE "PassageUID" = ${id}`;
        }

        const allTokens = await pool.query(query);
        res.json(allTokens.rows);
    } catch (error) {
        console.error(error.message);
    }
});

// GET TOKEN FREQUENCY
app.get("/token-frequency", async (req, res) => {
    try {

        let query = `SELECT COUNT(*) FROM "Token" t JOIN "Passage" p ON t."PassageUID" = p."PassageUID"`
        const lemma = req.query.lemma;
        const text = req.query.text;
        const work_id = req.query.work_id;

        // Add query to list
        let list = []
        if (lemma) {
            list.push(`t."Lemma" ILIKE '${lemma}'`);
        } else if (text) {
            list.push(`t."Text" ILIKE '${text}'`);
        } else if (work_id) {
            list.push(`p."WorkUID" = ${work_id}`);
        }

        // Add where to query
        if (list.length > 0) {
            query += ` WHERE ${list.join(" AND ")}`;
        }

        const allTokens = await pool.query(query);
        res.json(allTokens.rows[0]);
    } catch (error) {
        console.error(error.message);
    }
});

// GET APPEARANCE
app.get("/appearance", async (req, res) => {
    try {
        let query = `SELECT * FROM "Appearance" a JOIN "Character" c ON a."CharacterUID" = c."CharacterUID"`;
        const token_id = req.query.token_id;
        const work_id = req.query.work_id;

        // Add queryies
        if (token_id && work_id) {
            query += ` WHERE a."TokenUID" = ${token_id} AND c."WorkUID" = ${work_id}`;
        }
        else if (token_id) {
            query += ` WHERE a."TokenUID" = ${token_id}`;
        }
        else if (work_id) {
            query += ` AND c."WorkUID" = ${work_id}`;
        }

        const allAppearances = await pool.query(query);
        res.json(allAppearances.rows);
    } catch (error) {
        console.error(error.message);
    }
});

// GET WORD COUNT
app.get("/word-count", async (req, res) => {
    try {
        let query = `SELECT COUNT(*) FROM "Token" t JOIN "Passage" p ON t."PassageUID" = p."PassageUID" WHERE t."Alpha" = true`;
        const work_id = req.query.work_id;

        // Add query to list
        if (work_id) {
            query += ` AND p."WorkUID" = ${work_id}`;
        }

        const allTokens = await pool.query(query);
        res.json(allTokens.rows[0]);
    } catch (error) {
        console.error(error.message);
    }
});

// GET STOP WORD COUNT
app.get("/stop-word-count", async (req, res) => {
    try {
        let query = `SELECT COUNT(*) FROM "Token" t WHERE t."Alpha" = true AND t."Stop" = true`;
        const work_id = req.query.work_id;

        // Add query
        if (work_id) {
            query += ` AND t."WorkUID" = ${work_id}`;
        }

        const allTokens = await pool.query(query);
        res.json(allTokens.rows[0]);
    } catch (error) {
        console.error(error.message);
    }
});

// GET UNIQUE WORD COUNT
app.get("/unique-word-count", async (req, res) => {
    try {
        let query = `SELECT COUNT(DISTINCT t."Text") FROM "Token" t JOIN "Passage" p ON t."PassageUID" = p."PassageUID" WHERE t."Alpha" = true`;
        const work_id = req.query.work_id;

        // Add query
        if (work_id) {
            query += ` AND p."WorkUID" = ${work_id}`;
        }

        const allTokens = await pool.query(query);
        res.json(allTokens.rows[0]);
    } catch (error) {
        console.error(error.message);
    }
});

// GET UNIQUE CHARACTER COUNT
app.get("/unique-character-count", async (req, res) => {
    try {
        let query = `SELECT COUNT(DISTINCT c."Name") FROM "Appearance" a JOIN "Character" c ON a."CharacterUID" = c."CharacterUID"`;
        const work_id = req.query.work_id;

        // Add query
        if (work_id) {
            query += ` WHERE c."WorkUID" = ${work_id}`;
        }

        const allTokens = await pool.query(query);
        res.json(allTokens.rows[0]);
    } catch (error) {
        console.error(error.message);
    }
});

// Server start
app.listen(5000, () => {
    console.log("server has started on port 5000");
});