const { readIniFileSync } = require('read-ini-file')
const path = require('path')
const Pool = require("pg").Pool;

const root_path = path.normalize(path.join(__dirname, '..'))
const config_path = path.join(root_path, 'database.ini')
console.log(`Reading database config from ${config_path}`)
const config = readIniFileSync(config_path)
console.log(config.postgresql.host)

const pool = new Pool({
    user: config.postgresql.user,
    password: config.postgresql.password,
    host: config.postgresql.host,
    port: config.postgresql.port,
    database: config.postgresql.database
});

module.exports = pool;